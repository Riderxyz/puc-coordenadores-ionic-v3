import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map'
@Injectable()
export class LoginSrv {
    UrlProd = 'http://139.82.24.10/MobServ/';
    UrlDev = 'http://localhost:8100/';
    ApiLogin = '/api/usuarios?_usuario=';
loading:any
    constructor(public http: HttpClient, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
        console.log('Hello LoginSrvProvider Provider');

      this.loading = this.loadingCtrl.create({
        content: 'Iniciando...',
        spinner:'bubbles'
    });
    }

    AuthLogin(data) {
        this.loading.present();
        return this.http.get(this.UrlProd + this.ApiLogin + data.username + '&_senha=' + data.password).toPromise().then((login: any) => {
            if (login[0] == null) {
                this.loading.dismiss()
                this.presentToast()
            } else {
                const resultadoFinal = JSON.parse(login);
                return resultadoFinal.tabUsuario[0];
            }
        }).catch((err) => {
            console.log('erro', err);
            this.loading.dismiss()
            this.presentToast()
        })

    }


    presentToast() {
        let toast = this.toastCtrl.create({
            message: 'Erro de login. Usuario não cadastrado',
            duration: 3000,
            position: 'middle'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }
}
