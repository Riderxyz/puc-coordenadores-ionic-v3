import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import 'rxjs/add/operator/map'
/*
  Generated class for the UtilSrvProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilSrv {

    Dados = {
        dataInicial: new BehaviorSubject<string>(null),
        dataFinal: new BehaviorSubject<string>(null),
        nomeProjeto: new BehaviorSubject<string>(null),
        nomeUsuario: new BehaviorSubject<string>(null),
        codigoUsuario: new BehaviorSubject<string>(null),
        codigoProjeto: new BehaviorSubject<string>(null),
        contaPrincipal: new BehaviorSubject<string>(null),
        email: new BehaviorSubject<string>(null),
        listaProjeto: new BehaviorSubject<any>(null)
    }
    Mudar = {
        dataInicial: this.Dados.dataInicial.asObservable(),
        dataFinal: this.Dados.dataFinal.asObservable(),
        nomeProjeto: this.Dados.nomeProjeto.asObservable(),
        nomeUsuario: this.Dados.nomeUsuario.asObservable(),
        codigoUsuario: this.Dados.codigoUsuario.asObservable(),
        codigoProjeto: this.Dados.codigoProjeto.asObservable(),
        contaPrincipal: this.Dados.contaPrincipal.asObservable(),
        email: this.Dados.email.asObservable(),
        listaProjeto: this.Dados.listaProjeto.asObservable()
    }
    constructor(public http: HttpClient) {
        console.log('Hello UtilSrvProvider Provider');
    }
    SetLoginData(dados) {
        this.Dados.codigoUsuario.next(dados.coordenador);
        this.Dados.nomeUsuario.next(dados.nome);
        this.Dados.email.next(dados.email);
    }
    SetDate(dados) {
        if (dados.data === 1) {
            this.Dados.dataInicial.next(dados.novoValor);
        } else {
            this.Dados.dataFinal.next(dados.novoValor);
        }
    }
    SetProjectData(dados) {
        this.Dados.codigoProjeto.next(dados.codigo)
        this.Dados.nomeProjeto.next(dados.projeto.replace(/\s+/g, ' ').trim())
        this.Dados.contaPrincipal.next(dados.conta_principal)

        /*         codigo: 6623
                conta_principal: "32018.000"
                coordenador: 2241
                descricao: "1 - ADM = 10%"
                projeto: "LAB/GPSI/APLIC                     "
                tipo_projeto: "PR" */
    }
}
