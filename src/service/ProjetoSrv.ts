import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map'
import { UtilSrv } from './utilSrv';
@Injectable()
export class ProjetoSrv {
    UrlProd = 'http://139.82.24.10/MobServ/';
    UrlDev = 'http://localhost:8100/';
    ApiListaProjeto: string = '/api/projetos?coordenador=';
    ApiTotalProjeto: string = 'api/extratos/GetSaldosContas/coordenador/'
    codigo: any;
    data: string;
    constructor(public http: HttpClient, public UtilSrv: UtilSrv) {
        console.log('Hello Projeto Provider');
        this.UtilSrv.Mudar.codigoUsuario.subscribe((code) => {
            this.codigo = code;
        })
        this.UtilSrv.Mudar.dataInicial.subscribe((data) => {
            this.data = data;
        })

    }


    getListaProjetos() {
        const d = new Date()
        const parmdata = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate()
        return this.http.get(this.UrlProd + this.ApiListaProjeto +
            this.codigo +
            "&data=" + parmdata).map((projeto: string) => {
                const resultadoFinal = JSON.parse(projeto);
                return resultadoFinal.tabProjetos
            })
    }
    getTotalProjeto() {
        return this.http.get(this.UrlProd +
            this.ApiTotalProjeto +
            this.codigo + '/data/' + this.data).map((saldo: string) => {
                const resultadoFinal = JSON.parse(saldo);
                return resultadoFinal.tabSaldoContas[0]
            })
    }

}
