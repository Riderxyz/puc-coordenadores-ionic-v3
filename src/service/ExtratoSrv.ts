import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilSrv } from './utilSrv';
import 'rxjs/add/operator/map'
@Injectable()
export class ExtratoSrv {
    UrlProd = 'http://139.82.24.10/MobServ/';
    UrlDev = 'http://localhost:8100/';
    ApiExtrato: 'api/extratos/getExtrato/projeto/';
    ApiDetalheProjeto = 'api/extratos/getExtrato/projeto/';
    data = { inicial: null, final: null }
    projeto: string
    constructor(public http: HttpClient, public UtilSrv: UtilSrv) {
        console.log('Hello LoginSrvProvider Provider');
        this.UtilSrv.Mudar.dataInicial.subscribe((s) => {
            this.data.inicial = s
        })
        this.UtilSrv.Mudar.dataFinal.subscribe((s) => {
            this.data.final = s
        })
        this.UtilSrv.Mudar.codigoProjeto.subscribe((s) => {
            this.projeto = s
        })
    }

    getExtrato() {
        return this.http.get(this.UrlProd +
            this.ApiDetalheProjeto +
            this.projeto +
            "/di/" + this.data.inicial +
            "/df/" + this.data.final +
            "/pagina/1/pagina_tamanho/9999").map((extrato: any) => {
                const resultadoFinal = JSON.parse(extrato);
                return resultadoFinal.tabExtrato
            })



        //this.http.get(this.UrlProd + this.Api.Extrato + UsuarioSrv.getProjeto() + "/di/" + $scope.di + "/df/" + $scope.df + "/pagina/1/pagina_tamanho/9999")
    }

}
