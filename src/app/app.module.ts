import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ExtratoPage } from '../pages/extrato/extrato';
import { LoginPage } from '../pages/login/login';
import { ListaProjetosPage } from '../pages/lista-projetos/lista-projetos';
import { TotalProjetosPage } from '../pages/total-projetos/total-projetos';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UtilSrv } from '../service/utilSrv';
import { LoginSrv } from '../service/LoginSrv';
import { ExtratoSrv } from '../service/ExtratoSrv';
import { ProjetoSrv } from '../service/ProjetoSrv';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ExtratoPage,
    LoginPage,
    ListaProjetosPage,
    TotalProjetosPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    CommonModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ExtratoPage,
    LoginPage,
    ListaProjetosPage,
    TotalProjetosPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UtilSrv,
    LoginSrv,
    ExtratoSrv,
    ProjetoSrv,
    HttpClientModule,
  ]
})
export class AppModule { }
