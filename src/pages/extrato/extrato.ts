import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ExtratoSrv } from '../../service/ExtratoSrv';
ExtratoSrv
/**
 * Generated class for the ExtratoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-extrato',
  templateUrl: 'extrato.html',
})
export class ExtratoPage {
  ListaExtrato: any
  Date: any;
  Saldo: any
  constructor(public navCtrl: NavController, public navParams: NavParams, public ExtratoSrv: ExtratoSrv) {
    //   console.log(this.Dados);
    this.carregar()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExtratoPage');
  }
  carregar() {
    this.ExtratoSrv.getExtrato().subscribe((data: any) => {
      data.forEach(element => {
        element.nome = element.nome.replace(/\s+/g, ' ').trim();
        element.texto = element.texto.replace(/\s+/g, ' ').trim();
      });
      
      this.ListaExtrato = data;
      this.Saldo = data[data.length - 1].saldo
      console.log(this.ListaExtrato[this.ListaExtrato.length - 1]);
      /* data:"2014-09-02T00:00:00"
      despesa:0
      fatura:"          "
      id:299985
      linha:1
      nome:"LAB/GPSI/APLIC"
      projeto:"6623"
      receita:13887.74
      saldo:13887.74
      texto:"DE SIF AD 07 -> APLIC"
      valor:13887.74 */
    })
  }
  itemSelected(item) {
    console.log(item);

  }
}



