import { ExtratoPage } from './../extrato/extrato';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProjetoSrv } from '../../service/ProjetoSrv';
import { UtilSrv } from '../../service/utilSrv';

@IonicPage()
@Component({
  selector: 'page-lista-projetos',
  templateUrl: 'lista-projetos.html',
})
export class ListaProjetosPage {
  ListaProjetos: Array<any> = [];
  UserData: any;
  Date: any;
  codigo = 2241
  constructor(public navCtrl: NavController, public navParams: NavParams, public ProjetoSrv: ProjetoSrv, public UtilSrv:UtilSrv) {
    this.Date = navParams.get("data");
    this.UserData = navParams.get("dadosUsuario");
    this.carregar()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaProjetosPage');
  }

  carregar() {
    this.ProjetoSrv.getListaProjetos().subscribe((s) => {
      this.ListaProjetos = s
    })
  }
  itemSelected(item) {
    console.log(item);
    this.UtilSrv.SetProjectData(item)
    this.navCtrl.push(ExtratoPage)
  }

}



