import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaProjetosPage } from './lista-projetos';

@NgModule({
  declarations: [
    ListaProjetosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaProjetosPage),
  ],
})
export class ListaProjetosPageModule {}
