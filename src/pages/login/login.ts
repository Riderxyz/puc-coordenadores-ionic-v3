import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginSrv } from '../../service/LoginSrv';
import { UtilSrv } from '../../service/utilSrv';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  UserData = { username: 2241, password: 464185 }
  constructor(public navCtrl: NavController, public navParams: NavParams, public LoginSrv: LoginSrv, public UtilSrv: UtilSrv) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  LogIn() {
    if (this.UserData.password === null || this.UserData.password == null) {
      console.log('Erro');
    } else {
 
  //    const data = { username: 2241, password: 464185 }
      this.LoginSrv.AuthLogin(this.UserData).then((s)=>{
        if (s == undefined) {
          console.log('Bem...n funcionou', s);         
        } else {
          this.LoginSrv.loading.dismiss()
          this.UtilSrv.SetLoginData(s)
          this.navCtrl.setRoot(HomePage)
        }
      })
    }
  }
}
