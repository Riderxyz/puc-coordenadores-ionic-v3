import { ListaProjetosPage } from './../lista-projetos/lista-projetos';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UtilSrv } from '../../service/utilSrv';
import { LoginSrv } from '../../service/LoginSrv';
import { ProjetoSrv } from '../../service/ProjetoSrv';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  buttons: any;
  ListaProjetos = [];
  Date = { inicial: null, final: null }
  UserData = { codigo: null, email: null, nome: null }
  Conta = { conta: '', descricao: '', saldo: '' }
  iox = ''
  constructor(public navCtrl: NavController, public ProjetoSrv: ProjetoSrv, public UtilSrv: UtilSrv, public LoginSrv: LoginSrv) {
    this.getButtons();
    console.log('Dados', this.Date)
    this.UtilSrv.Mudar.codigoUsuario.subscribe((s) => {
      this.iox = s
      console.log('Aqui é IOX!', this.iox);
    })

  }
  change(ev, type) {
    const change = { novoValor: ev, data: type }
    this.UtilSrv.SetDate(change)
    this.UtilSrv.Mudar.dataInicial.subscribe(() => {
      this.TotalConta()
    })
  }
  Listar() {
    if ((this.Date.inicial === null) || (this.Date.final === null)) {
      console.log('N vai funcionar');
    } else {
      this.navCtrl.push(ListaProjetosPage)
    }
  }
  TotalConta() {
    this.ProjetoSrv.getTotalProjeto().subscribe((resultado) => {
      this.Conta.conta = resultado.conta.replace(/\s+/g, ' ').trim();
      this.Conta.descricao = resultado.descricao.replace(/\s+/g, ' ').trim();
      this.Conta.saldo = resultado.saldo
    })
  }
  getButtons() {
    this.buttons = [{
      nome: 'Extratos',
      cor: 'primary',
      icon: 'logo-usd'
    },
    {
      nome: 'Saldo das Contas',
      cor: 'danger',
      icon: 'git-network'
    }]
  }
}
