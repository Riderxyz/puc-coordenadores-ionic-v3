import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TotalProjetosPage } from './total-projetos';

@NgModule({
  declarations: [
    TotalProjetosPage,
  ],
  imports: [
    IonicPageModule.forChild(TotalProjetosPage),
  ],
})
export class TotalProjetosPageModule {}
