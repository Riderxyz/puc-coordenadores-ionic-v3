webpackJsonp([4],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExtratoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_util_srv_util_srv__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ExtratoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ExtratoPage = /** @class */ (function () {
    function ExtratoPage(navCtrl, navParams, utilSrv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.utilSrv = utilSrv;
        this.Date = navParams.get("data");
        this.Dados = navParams.get("dadosProjeto");
        console.log(this.Dados);
        this.carregar();
    }
    ExtratoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ExtratoPage');
    };
    ExtratoPage.prototype.carregar = function () {
        console.log('Dentro da função');
        this.utilSrv.getExtrato(this.Dados, this.Date).then(function (data) {
            console.log(data);
        });
    };
    ExtratoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-extrato',template:/*ion-inline-start:"/Users/ons/Documents/teste117/delta/src/pages/extrato/extrato.html"*/'<!--\n  Generated template for the ExtratoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>extrato</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ons/Documents/teste117/delta/src/pages/extrato/extrato.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_util_srv_util_srv__["a" /* UtilSrvProvider */]])
    ], ExtratoPage);
    return ExtratoPage;
}());

//# sourceMappingURL=extrato.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaProjetosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__extrato_extrato__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ProjetoSrv__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




__WEBPACK_IMPORTED_MODULE_3__service_ProjetoSrv__["a" /* ProjetoSrv */];
var ListaProjetosPage = /** @class */ (function () {
    function ListaProjetosPage(navCtrl, navParams, ProjetoSrv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ProjetoSrv = ProjetoSrv;
        this.ListaProjetos = [];
        this.codigo = 2241;
        this.Date = navParams.get("data");
        this.UserData = navParams.get("dadosUsuario");
        this.carregar();
    }
    ListaProjetosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListaProjetosPage');
    };
    ListaProjetosPage.prototype.carregar = function () {
        var _this = this;
        console.log('O q eu preciso', this.codigo);
        this.ProjetoSrv.getListaProjetos(this.UserData.codigo).subscribe(function (s) {
            console.log('S de projeto', s);
            _this.ListaProjetos = s;
        });
    };
    ListaProjetosPage.prototype.itemSelected = function (item) {
        console.log(item);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__extrato_extrato__["a" /* ExtratoPage */], {
            dadosProjeto: item.codigo,
            data: this.Date
        });
    };
    ListaProjetosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-lista-projetos',template:/*ion-inline-start:"/Users/ons/Documents/teste117/delta/src/pages/lista-projetos/lista-projetos.html"*/'<!--\n  Generated template for the ListaProjetosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Lista Projetos</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n  <ion-list>\n    <ion-item *ngFor="let item of ListaProjetos" color="royal" (click)="itemSelected(item)">\n      <h2>Projeto: {{ item.projeto }}</h2>\n      <h3>Descrição: {{ item.descricao }}</h3>\n      <p>Conta Principal: {{item.conta_principal}}</p>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/ons/Documents/teste117/delta/src/pages/lista-projetos/lista-projetos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__service_ProjetoSrv__["a" /* ProjetoSrv */]])
    ], ListaProjetosPage);
    return ListaProjetosPage;
}());

//# sourceMappingURL=lista-projetos.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/ons/Documents/teste117/delta/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n\n</ion-header>\n\n\n<ion-content class="background-oficial">\n  <ion-list>\n    <ion-item>\n      <ion-label>Usuario</ion-label>\n      <ion-input type="text"></ion-input>\n    </ion-item>\n  \n    <ion-item>\n      <ion-label>Password</ion-label>\n      <ion-input type="password"></ion-input>\n    </ion-item>\n  </ion-list>\n  \n  <button block ion-button icon-start>\n    <ion-icon name="home"></ion-icon>\n   Entrar\n  </button>\n</ion-content>\n'/*ion-inline-end:"/Users/ons/Documents/teste117/delta/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 116:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 116;

/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/extrato/extrato.module": [
		286,
		3
	],
	"../pages/lista-projetos/lista-projetos.module": [
		287,
		2
	],
	"../pages/login/login.module": [
		288,
		1
	],
	"../pages/total-projetos/total-projetos.module": [
		289,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 157;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilSrvProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the UtilSrvProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UtilSrvProvider = /** @class */ (function () {
    function UtilSrvProvider(http) {
        this.http = http;
        this.Dados = {
            dataInicial: null,
            dataFinal: null,
            nomeProjeto: null,
            nomeUsuario: null,
            codigo: null,
            listaProjeto: null,
            projeto: null
        };
        this.UrlProd = 'http://139.82.24.10/MobServ/';
        this.UrlDev = 'http://localhost:8100/';
        this.Api = {
            login: '/api/usuarios?_usuario=',
            listaProjeto: '/api/projetos?coordenador=',
            Extrato: 'api/extratos/getExtrato/projeto/',
            DetalheProjeto: 'api/extratos/getExtrato/projeto/'
        };
        console.log('Hello UtilSrvProvider Provider');
    }
    UtilSrvProvider.prototype.getLogin = function (data) {
        var _this = this;
        return this.http.get(this.UrlProd + this.Api.login + data.username + '&_senha=' + data.password)
            .toPromise().then(function (login) {
            if (login[0] == null) {
                console.log(12);
            }
            else {
                var resultadoFinal = JSON.parse(login);
                _this.Dados.codigo = resultadoFinal.tabUsuario[0].coordenador;
                _this.Dados.nomeUsuario = resultadoFinal.tabUsuario[0].nome;
                return resultadoFinal.tabUsuario[0];
            }
        }).catch(function (err) {
            console.log(err);
            return err;
        });
    };
    UtilSrvProvider.prototype.getListaProjetos = function (codigo) {
        var d = new Date();
        var parmdata = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();
        return this.http.get(this.UrlProd +
            this.Api.listaProjeto +
            codigo +
            "&data=" + parmdata).toPromise().then(function (projetos) {
            var resultadoFinal = JSON.parse(projetos);
            return resultadoFinal.tabProjetos;
        });
    };
    UtilSrvProvider.prototype.getExtrato = function (projeto, data) {
        console.log(data);
        return this.http.get(this.UrlProd +
            this.Api.DetalheProjeto +
            projeto +
            "/di/" + data.inicial +
            "/df/" + data.final +
            "/pagina/1/pagina_tamanho/9999").toPromise().then(function (extrato) {
            console.log('Do serviço de extrato', extrato);
            var resultadoFinal = JSON.parse(extrato);
            return resultadoFinal.tabExtrato;
        });
        //this.http.get(this.UrlProd + this.Api.Extrato + UsuarioSrv.getProjeto() + "/di/" + $scope.di + "/df/" + $scope.df + "/pagina/1/pagina_tamanho/9999")
    };
    UtilSrvProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], UtilSrvProvider);
    return UtilSrvProvider;
}());

//# sourceMappingURL=util-srv.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjetoSrv; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProjetoSrv = /** @class */ (function () {
    function ProjetoSrv(http) {
        this.http = http;
        this.UrlProd = 'http://139.82.24.10/MobServ/';
        this.UrlDev = 'http://localhost:8100/';
        console.log('Hello Projeto Provider');
    }
    ProjetoSrv.prototype.getListaProjetos = function (codigo) {
        var d = new Date();
        var parmdata = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();
        return this.http.get(this.UrlProd + '/api/projetos?coordenador=' +
            codigo +
            "&data=" + parmdata).map(function (projeto) {
            var resultadoFinal = JSON.parse(projeto);
            console.log('O q eu preciso', resultadoFinal);
            return resultadoFinal.tabProjetos;
        });
        /*     .toPromise().then((projetos: any) => {
                const resultadoFinal = JSON.parse(projetos);
                return resultadoFinal.tabProjetos
            }) */
    };
    ProjetoSrv = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ProjetoSrv);
    return ProjetoSrv;
}());

//# sourceMappingURL=ProjetoSrv.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilSrv; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the UtilSrvProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UtilSrv = /** @class */ (function () {
    function UtilSrv(http) {
        this.http = http;
        this.Dados = {
            dataInicial: null,
            dataFinal: null,
            nomeProjeto: null,
            nomeUsuario: null,
            codigo: null,
            listaProjeto: null,
            projeto: null
        };
        this.UrlProd = 'http://139.82.24.10/MobServ/';
        this.UrlDev = 'http://localhost:8100/';
        this.Api = {
            login: '/api/usuarios?_usuario=',
            listaProjeto: '/api/projetos?coordenador=',
            Extrato: 'api/extratos/getExtrato/projeto/',
            DetalheProjeto: 'api/extratos/getExtrato/projeto/'
        };
        console.log('Hello UtilSrvProvider Provider');
    }
    UtilSrv.prototype.getLogin = function (data) {
        var _this = this;
        return this.http.get(this.UrlProd + this.Api.login + data.username + '&_senha=' + data.password)
            .toPromise().then(function (login) {
            if (login[0] == null) {
                console.log(12);
            }
            else {
                var resultadoFinal = JSON.parse(login);
                _this.Dados.codigo = resultadoFinal.tabUsuario[0].coordenador;
                _this.Dados.nomeUsuario = resultadoFinal.tabUsuario[0].nome;
                return resultadoFinal.tabUsuario[0];
            }
        }).catch(function (err) {
            console.log(err);
            return err;
        });
    };
    UtilSrv.prototype.getListaProjetos = function (codigo) {
        var d = new Date();
        var parmdata = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();
        return this.http.get(this.UrlProd +
            this.Api.listaProjeto +
            codigo +
            "&data=" + parmdata).toPromise().then(function (projetos) {
            var resultadoFinal = JSON.parse(projetos);
            return resultadoFinal.tabProjetos;
        });
    };
    UtilSrv.prototype.getExtrato = function (projeto, data) {
        console.log(data);
        return this.http.get(this.UrlProd +
            this.Api.DetalheProjeto +
            projeto +
            "/di/" + data.inicial +
            "/df/" + data.final +
            "/pagina/1/pagina_tamanho/9999").toPromise().then(function (extrato) {
            console.log('Do serviço de extrato', extrato);
            var resultadoFinal = JSON.parse(extrato);
            return resultadoFinal.tabExtrato;
        });
        //this.http.get(this.UrlProd + this.Api.Extrato + UsuarioSrv.getProjeto() + "/di/" + $scope.di + "/df/" + $scope.df + "/pagina/1/pagina_tamanho/9999")
    };
    UtilSrv = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], UtilSrv);
    return UtilSrv;
}());

//# sourceMappingURL=utilSrv.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginSrv; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginSrv = /** @class */ (function () {
    function LoginSrv(http) {
        this.http = http;
        this.UrlProd = 'http://139.82.24.10/MobServ/';
        this.UrlDev = 'http://localhost:8100/';
        this.ApiLogin = '/api/usuarios?_usuario=';
        console.log('Hello LoginSrvProvider Provider');
    }
    LoginSrv.prototype.AuthLogin = function (data) {
        return this.http.get(this.UrlProd + this.ApiLogin + data.username + '&_senha=' + data.password).map(function (login) {
            console.log(login);
            if (login[0] == null) {
                console.log(12);
            }
            else {
                console.log(login);
                var resultadoFinal = JSON.parse(login);
                //this.Dados.codigo = resultadoFinal.tabUsuario[0].coordenador;
                // this.Dados.nomeUsuario = resultadoFinal.tabUsuario[0].nome;
                console.log(resultadoFinal);
                return resultadoFinal.tabUsuario[0];
            }
        });
    };
    LoginSrv = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], LoginSrv);
    return LoginSrv;
}());

//# sourceMappingURL=LoginSrv.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TotalProjetosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TotalProjetosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TotalProjetosPage = /** @class */ (function () {
    function TotalProjetosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TotalProjetosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TotalProjetosPage');
    };
    TotalProjetosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-total-projetos',template:/*ion-inline-start:"/Users/ons/Documents/teste117/delta/src/pages/total-projetos/total-projetos.html"*/'<!--\n  Generated template for the TotalProjetosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>TotalProjetos</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ons/Documents/teste117/delta/src/pages/total-projetos/total-projetos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], TotalProjetosPage);
    return TotalProjetosPage;
}());

//# sourceMappingURL=total-projetos.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(227);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_extrato_extrato__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_lista_projetos_lista_projetos__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_total_projetos_total_projetos__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_util_srv_util_srv__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_http__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__service_LoginSrv__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__service_ExtratoSrv__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__service_ProjetoSrv__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__service_utilSrv__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_extrato_extrato__["a" /* ExtratoPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_lista_projetos_lista_projetos__["a" /* ListaProjetosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_total_projetos_total_projetos__["a" /* TotalProjetosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/extrato/extrato.module#ExtratoPageModule', name: 'ExtratoPage', segment: 'extrato', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lista-projetos/lista-projetos.module#ListaProjetosPageModule', name: 'ListaProjetosPage', segment: 'lista-projetos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/total-projetos/total-projetos.module#TotalProjetosPageModule', name: 'TotalProjetosPage', segment: 'total-projetos', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_extrato_extrato__["a" /* ExtratoPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_lista_projetos_lista_projetos__["a" /* ListaProjetosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_total_projetos_total_projetos__["a" /* TotalProjetosPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_12__providers_util_srv_util_srv__["a" /* UtilSrvProvider */],
                __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_15__service_LoginSrv__["a" /* LoginSrv */],
                __WEBPACK_IMPORTED_MODULE_16__service_ExtratoSrv__["a" /* ExtratoSrv */],
                __WEBPACK_IMPORTED_MODULE_17__service_ProjetoSrv__["a" /* ProjetoSrv */],
                __WEBPACK_IMPORTED_MODULE_18__service_utilSrv__["a" /* UtilSrv */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/ons/Documents/teste117/delta/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/ons/Documents/teste117/delta/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lista_projetos_lista_projetos__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_utilSrv__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_LoginSrv__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, utilSrv, LoginSrv) {
        this.navCtrl = navCtrl;
        this.utilSrv = utilSrv;
        this.LoginSrv = LoginSrv;
        this.ListaProjetos = [];
        this.DateX = { inicial: null, final: null };
        this.UserData = { codigo: null, email: null, nome: null };
        this.getButtons();
    }
    HomePage.prototype.logs = function () {
        var _this = this;
        var data = { username: 2241, password: 464185 };
        console.log(data);
        this.LoginSrv.AuthLogin(data).subscribe(function (s) {
            console.log('Auth => ', s);
            _this.UserData.codigo = s.coordenador;
            _this.UserData.email = s.email;
            _this.UserData.nome = s.nome;
        });
    };
    HomePage.prototype.Listar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__lista_projetos_lista_projetos__["a" /* ListaProjetosPage */], {
            dadosUsuario: this.UserData,
            data: this.DateX
        });
    };
    HomePage.prototype.getButtons = function () {
        this.buttons = [{
                nome: 'Extratos',
                cor: 'primary',
                icon: 'logo-usd'
            },
            {
                nome: 'Saldo das Contas',
                cor: 'danger',
                icon: 'git-network'
            }];
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/ons/Documents/teste117/delta/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title text-center>\n      Fundação Padre Leonel Franca\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="background">\n  <ion-toolbar full text-center color="energized">\n    <ion-title>Controle de Projetos</ion-title>\n  </ion-toolbar>\n  <div *ngFor="let item of buttons">\n    <button ion-button block (click)="Listar()" color="{{item.cor}}">\n      <ion-icon name={{item.icon}}></ion-icon>\n      {{item.nome}}\n    </button>\n  </div>\n  <br>\n  <br>\n  <br>\n  <br>\n  <br>\n  <div class="center" hidden="true">\n    <!-- <img src="" text-center alt=""> -->\n  </div>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n        <ion-item color="royal" class="redondo">\n          <ion-label color="light">Inicio</ion-label>\n          <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="DateX.inicial"></ion-datetime>\n        </ion-item>\n      </ion-col>\n      <ion-col col-6>\n        <ion-item color="light" class="redondo">\n          <ion-label color="dark">Fim</ion-label>\n          <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="DateX.final" color="dark"></ion-datetime>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-footer>\n    <button ion-button block (click)="logs()" color="dark">Sair</button>\n  </ion-footer>\n</ion-content>'/*ion-inline-end:"/Users/ons/Documents/teste117/delta/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__service_utilSrv__["a" /* UtilSrv */], __WEBPACK_IMPORTED_MODULE_4__service_LoginSrv__["a" /* LoginSrv */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExtratoSrv; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExtratoSrv = /** @class */ (function () {
    function ExtratoSrv(http) {
        this.http = http;
        this.UrlProd = 'http://139.82.24.10/MobServ/';
        this.UrlDev = 'http://localhost:8100/';
        this.ApiDetalheProjeto = 'api/extratos/getExtrato/projeto/';
        console.log('Hello LoginSrvProvider Provider');
    }
    ExtratoSrv.prototype.getExtrato = function (projeto, data) {
        console.log(data);
        return this.http.get(this.UrlProd +
            this.ApiDetalheProjeto +
            projeto +
            "/di/" + data.inicial +
            "/df/" + data.final +
            "/pagina/1/pagina_tamanho/9999").toPromise().then(function (extrato) {
            console.log('Do serviço de extrato', extrato);
            var resultadoFinal = JSON.parse(extrato);
            return resultadoFinal.tabExtrato;
        });
        //this.http.get(this.UrlProd + this.Api.Extrato + UsuarioSrv.getProjeto() + "/di/" + $scope.di + "/df/" + $scope.df + "/pagina/1/pagina_tamanho/9999")
    };
    ExtratoSrv = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ExtratoSrv);
    return ExtratoSrv;
}());

//# sourceMappingURL=ExtratoSrv.js.map

/***/ })

},[206]);
//# sourceMappingURL=main.js.map